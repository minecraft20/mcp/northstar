package dk.n3xuz.hookregistry;

import dk.n3xuz.hookregistry.context.Cancellable;
import dk.n3xuz.hookregistry.context.Context;
import dk.n3xuz.hookregistry.filters.Filter;
import dk.n3xuz.hookregistry.filters.defaults.CancellableFilter;
import sun.reflect.generics.reflectiveObjects.ParameterizedTypeImpl;

import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Objects;

public abstract class Hook<T extends Context> {
    protected final String label;
    protected final Class<T> contextType;
    protected final Priority priority;
    protected Filter<T>[] filters;

    public Hook(String label) {
        this(label, Priority.NORMAL);
    }

    @SuppressWarnings("unchecked")
    public Hook(String label, Priority priority) {
        this.label = label;
        this.priority = priority;
        Class<T> contextType = null;
        Type generic = getClass().getGenericSuperclass();
        if (generic instanceof ParameterizedTypeImpl)
            for (Type type : ((ParameterizedTypeImpl) generic).getActualTypeArguments())
                if (type instanceof Class
                        && Context.class.isAssignableFrom((Class<?>) type)) {
                    contextType = (Class<T>) type;
                    break;
                }
        Objects.requireNonNull(this.contextType = contextType, "Failed to resolve context type. (context not declared or too complex?)");
        if (Cancellable.class.isAssignableFrom(contextType))
            addFilter((Filter<T>) new CancellableFilter<>());
    }

    public abstract void call(T context);

    public final Priority getPriority() {
        return Priority.NORMAL;
    }

    public final Filter<T>[] getFilters() {
        return filters;
    }

    @SuppressWarnings("unchecked")
    public final void addFilter(Filter<T> filter) {
        Objects.requireNonNull(filter, "Filter cannot be null");
        (filters = Objects.isNull(filters) ? (Filter<T>[]) new Filter<?>[1]
                : Arrays.copyOf(filters, filters.length + 1))
                [filters.length - 1] = filter;
    }

    public final Class<T> getContextType() {
        return contextType;
    }

    public final String getLabel() {
        return label;
    }
}
