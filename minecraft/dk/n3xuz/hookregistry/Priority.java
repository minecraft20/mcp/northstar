package dk.n3xuz.hookregistry;

public enum Priority {
    PRE_MONITOR,
    HIGHEST,
    HIGH,
    NORMAL,
    LOW,
    LOWEST,
    POST_MONITOR;

    public final boolean monitor = name().contains("MONITOR");
}
