package dk.n3xuz.hookregistry.context;

public abstract class CancellableContext extends Context implements Cancellable {
    protected boolean cancelled;

    @Override
    public boolean isCancelled() {
        return cancelled;
    }

    @Override
    public void setCancelled(boolean flag) {
        checkLock();
        cancelled = flag;
    }

    @Override
    public void cancel() {
        setCancelled(true);
    }
}
