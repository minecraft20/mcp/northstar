package dk.n3xuz.hookregistry.context;

public interface Cancellable {
    boolean isCancelled();

    void setCancelled(boolean flag);

    void cancel();
}
