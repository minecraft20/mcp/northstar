package dk.n3xuz.hookregistry.filters.defaults;

import dk.n3xuz.hookregistry.Hook;
import dk.n3xuz.hookregistry.context.Cancellable;
import dk.n3xuz.hookregistry.context.Context;
import dk.n3xuz.hookregistry.filters.Filter;

public final class CancellableFilter<T extends Context & Cancellable> implements Filter<T> {

    @Override
    public boolean test(Hook<T> hook, T context) {
        return !context.isCancelled();
    }
}
