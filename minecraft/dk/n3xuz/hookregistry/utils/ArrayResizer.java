package dk.n3xuz.hookregistry.utils;

import static java.lang.System.arraycopy;
import static java.lang.reflect.Array.newInstance;
import static java.util.Arrays.copyOf;
import static java.util.Arrays.copyOfRange;

public final class ArrayResizer {
    private ArrayResizer() {
    }

    public static <T> T[] remove(T[] array, T t) {
        for (int index = 0; index < array.length; index++)
            if (array[index] == t)
                return remove(array, index);
        return array;
    }

    @SuppressWarnings("unchecked")
    public static <T> T[] remove(T[] array, int index) {
        if (index <= 0)
            return copyOfRange(array, 1, array.length);
        else if (index >= array.length)
            return copyOf(array, array.length - 1);
        T[] newArray = (T[]) newInstance(array.getClass().getComponentType(), array.length - 1);
        arraycopy(array, 0, newArray, 0, index);
        arraycopy(array, index + 1, newArray, index, newArray.length - index);
        return newArray;
    }

    @SafeVarargs
    public static <T> T[] insert(T[] array, int index, T... t) {
        if (index <= 0)
            return prepend(array, t);
        else if (index >= array.length)
            return append(array, t);
        T[] newArray = copyOf(array, array.length + t.length);
        arraycopy(t, 0, newArray, index, t.length);
        arraycopy(array, index, newArray, index + t.length, array.length - index);
        return newArray;
    }

    @SafeVarargs
    public static <T> T[] append(T[] array, T... t) {
        return concat(array, t);
    }

    @SafeVarargs
    public static <T> T[] prepend(T[] array, T... t) {
        return concat(t, array);
    }

    public static <T> T[] concat(T[] first, T[] second) {
        T[] newArray = copyOf(first, first.length + second.length);
        arraycopy(second, 0, newArray, first.length, second.length);
        return newArray;
    }
}
