package dk.n3xuz.hookregistry;

import dk.n3xuz.hookregistry.context.Context;

public interface HookRegistry {
    boolean register(Hook<?>... hook);

    boolean unregister(Hook<?>... hook);

    <T extends Context> T dispatch(T context);
}
