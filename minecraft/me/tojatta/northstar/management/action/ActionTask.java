package me.tojatta.northstar.management.action;

/**
 * Created by Tojatta on 3/1/2016.
 */
public abstract class ActionTask {

    public abstract void invoke();

}
