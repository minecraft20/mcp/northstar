package me.tojatta.northstar.management.action;

import org.lwjgl.input.Keyboard;

/**
 * Created by Tojatta on 3/1/2016.
 */
public abstract class MultiKeyActionTask extends ActionTask {

    public abstract boolean isKeyAllowed(int key);

    public abstract void invoke(int invokedKey);

    @Override
    public void invoke() {
        invoke(Keyboard.KEY_NONE);
    }
}
