package me.tojatta.northstar.management.action;

import dk.n3xuz.hookregistry.Hook;
import me.tojatta.northstar.Northstar;
import me.tojatta.northstar.hooks.ActionContext;
import me.tojatta.northstar.interfaces.Loadable;
import me.tojatta.northstar.management.MapManager;
import me.tojatta.northstar.management.action.tasks.ToggleTask;
import org.lwjgl.input.Keyboard;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by Tojatta on 3/1/2016.
 */
public class ActionManager extends MapManager<Action[], ActionTask[]> implements Loadable {

    public ActionManager() {
        setup();
        Northstar.getInstance().getHookRegistry().register(new Hook<ActionContext.Keypress>("action_keypress_hook") {

            @Override
            public void call(ActionContext.Keypress context) {
                if (context.getKey() != -1) {
                    for (ActionTask taskx : getAllTaskForAction(Action.KEY_PRESS)) {

                        if (taskx instanceof MultiKeyActionTask) {

                        } else if (taskx != null) {
                            Optional<ActionTask> toggleTask = getAllTaskForAction(Action.KEY_PRESS).stream().filter(task -> task instanceof ToggleTask)
                                    .filter(task -> Keyboard.getKeyIndex(((ToggleTask) task).getModule().getKey()) == context.getKey()).findFirst();
                            if (toggleTask.isPresent()) {
                                toggleTask.get().invoke();
                                return;
                            }
                        }
                    }
                }
            }

        });

        Northstar.getInstance().getHookRegistry().register(new Hook<ActionContext.MiddleClick>("action_middleclick_hook") {

            @Override
            public void call(ActionContext.MiddleClick context) {
                //TODO: Add friend adding in here.
            }

        });
    }

    /**
     * Returns all tasks for given action.
     * @param action
     * @return
     */
    public List<ActionTask> getAllTaskForAction(Action action) {
        List<ActionTask> taskList = new ArrayList<>();
        for (Map.Entry<Action[], ActionTask[]> entry : map.entrySet()) {
            for (Action act : entry.getKey()) {
                if (act.equals(action)) {
                    taskList.addAll(Arrays.asList(entry.getValue()));
                }
            }
        }
        return taskList;
    }

    @Override
    public void setup() {
        this.map = new ConcurrentHashMap<>();
    }

    @Override
    public boolean load() {
        return false;
    }

    @Override
    public boolean save() {
        return false;
    }
}
