package me.tojatta.northstar.management.module;

import me.tojatta.northstar.Northstar;
import me.tojatta.northstar.management.action.Action;
import me.tojatta.northstar.management.action.ActionTask;
import me.tojatta.northstar.management.action.tasks.ToggleTask;
import me.tojatta.northstar.management.module.annotations.ModuleManifest;
import me.tojatta.northstar.management.module.interfaces.Toggleable;

/**
 * Created by Tojatta on 2/26/2016.
 */
public class ToggleableModule extends Module implements Toggleable {

    /**
     * Toggleable module name aliase(s).
     */
    private String[] aliases;

    /**
     * Toggleable module keybind.
     */
    private String key;

    /**
     * Toggleable module category.
     */
    private ModuleCategory category;

    /**
     * Toggleable module state.
     */
    private boolean running;

    /**
     * Toggleable module constructor.
     */
    public ToggleableModule() {
        this.name = this.getClass().getAnnotation(ModuleManifest.class).name();                 //Setting module name from ModuleManifest annotation.
        this.aliases = this.getClass().getAnnotation(ModuleManifest.class).aliases();           //Setting toggleable module aliases from ModuleManifest annotation.
        this.description = this.getClass().getAnnotation(ModuleManifest.class).description();   //Setting toggleable module description from ModuleManifest annotation.
        this.author = this.getClass().getAnnotation(ModuleManifest.class).author();             //Setting toggleable module author from ModuleManifest annotation.
        this.key = this.getClass().getAnnotation(ModuleManifest.class).key();                   //Setting toggleable module key from ModuleManifest annotation.
        this.category = this.getClass().getAnnotation(ModuleManifest.class).category();         //Setting toggleable module category from ModuleManifest annotation.
        Northstar.getInstance().getActionManager().getMap().put(new Action[]{Action.KEY_PRESS}, new ActionTask[]{new ToggleTask(this)});
    }

    /**
     * Returns running state for the toggleable module.
     *
     * @return running
     */
    @Override
    public boolean isRunning() {
        return running;
    }

    /**
     * Gets called when the toggleable module is enabled.
     */
    @Override
    public void onEnable() {

    }

    /**
     * Gets called when the toggleable module is disabled.
     */
    @Override
    public void onDisable() {

    }

    /**
     * Gets called when the toggleable module is toggled.
     */
    @Override
    public void onToggle() {

    }

    /**
     * Toggles the module.
     */
    @Override
    public void toggle() {
        setState(!running); //Sets the module state to the opposite of it's current state.
        onToggle();         //Calls onToggle.
    }

    /**
     * Sets the toggleable module state.
     *
     * @param state
     */
    public void setState(boolean state) {
        running = state;
        if (running) {
            onEnable();     //Calls onEnable when turning the module state true.
        } else {
            onDisable();    //Calls onDisable when turning the module state false.
        }
    }

    /**
     * Returns toggleable module aliases.
     *
     * @return aliases
     */
    public String[] getAliases() {
        return aliases;
    }

    /**
     * Sets toggleable module aliases.
     *
     * @param aliases
     */
    public void setAliases(String[] aliases) {
        this.aliases = aliases;
    }

    /**
     * Returns toggleable module keybind.
     *
     * @return key
     */
    public String getKey() {
        return key;
    }

    /**
     * Sets toggleable module keybind.
     *
     * @param key
     */
    public void setKey(String key) {
        this.key = key;
    }

    /**
     * Returns toggleable module category type.
     *
     * @return category
     */
    public ModuleCategory getCategory() {
        return category;
    }

    /**
     * Sets toggleable module category type.
     *
     * @param category
     */
    public void setCategory(ModuleCategory category) {
        this.category = category;
    }
}
