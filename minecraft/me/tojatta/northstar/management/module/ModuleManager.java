package me.tojatta.northstar.management.module;

import me.tojatta.northstar.interfaces.Loadable;
import me.tojatta.northstar.management.ListManager;
import me.tojatta.northstar.management.module.modules.client.Hud;

import java.util.ArrayList;

/**
 * Created by Tojatta on 2/26/2016.
 */
public class ModuleManager extends ListManager<Module> implements Loadable {

    /**
     * Module Manager constructor.
     */
    public ModuleManager() {
        setup();            //Calls setup()
    }

    /**
     * Sets up contents for the ListManager.
     */
    @Override
    public void setup() {
        contents = new ArrayList<>();
        add(new Hud());
    }

    /**
     * Called when the client is loaded.
     *
     * @return success?
     */
    @Override
    public boolean load() {
        return false;
    }

    /**
     * Called when the client is closed.
     *
     * @return success?
     */
    @Override
    public boolean save() {
        return false;
    }
}
