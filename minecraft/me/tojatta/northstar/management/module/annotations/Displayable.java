package me.tojatta.northstar.management.module.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by Tojatta on 2/26/2016.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
public @interface Displayable {

    /**
     * Displayed color.
     *
     * @return color.
     */
    int color();

}
