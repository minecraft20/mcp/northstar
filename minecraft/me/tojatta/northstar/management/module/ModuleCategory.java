package me.tojatta.northstar.management.module;

/**
 * Created by Tojatta on 2/26/2016.
 */
public enum ModuleCategory {
    /**
     * Types of modules.
     */
    CLIENT, COMBAT, MOVEMENT, RENDER
}
