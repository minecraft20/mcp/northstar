package me.tojatta.northstar.management;

import java.util.Map;

/**
 * Created by Tojatta on 3/1/2016.
 */
public abstract class MapManager<K, V> {

    protected Map<K, V> map;

    /**
     * Returns contents of the map.
     * @return
     */
    public Map<K, V> getMap() {
        return map;
    }

    /**
     * Abstract setup method for initializing Map<K, V> contents
     */
    public abstract void setup();

}
