package me.tojatta.northstar.hooks;

import dk.n3xuz.hookregistry.context.Context;
import net.minecraft.client.gui.ScaledResolution;

/**
 * Created by Tojatta on 3/1/2016.
 */
public class RenderContext extends Context {

    public static class Screen extends RenderContext {

        private ScaledResolution resolution;

        public Screen(ScaledResolution scaledResolution){
            resolution = scaledResolution;
        }

        public ScaledResolution getResolution() {
            return resolution;
        }
    }

    public static class World extends RenderContext {

    }

}
