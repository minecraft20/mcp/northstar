package me.tojatta.northstar.hooks;

import dk.n3xuz.hookregistry.context.Context;

/**
 * Created by Tojatta on 3/1/2016.
 */
public class ActionContext extends Context {

    public static class RightClick extends ActionContext {

    }

    public static class MiddleClick extends ActionContext {

    }

    public static class LeftClick extends ActionContext {

    }

    public static class Keypress extends ActionContext {

        private int key;

        public Keypress(int key) {
            this.key = key;
        }

        public int getKey() {
            return key;
        }

        public void setKey(int key) {
            this.key = key;
        }
    }
}
